var restify = require('restify');
var { log } = require('./logHelper');
const corsMiddleware = require('restify-cors-middleware');

server = restify.createServer({
  name: 'svc-stats',
  log: log
});

server.use(restify.plugins.queryParser());
server.use(restify.plugins.fullResponse());
server.use(restify.plugins.bodyParser());

// https://stackoverflow.com/questions/20626470/is-there-a-way-to-log-every-request-in-the-console-with-restify
server.pre(function(req, rsp, next) {
  req.log.info({req: req}, 'REQUEST');
  next();
})

var port = process.env.PORT || 8890;
server.listen(port, function() {
  log.info('%s listening at %s', server.name, server.url);
});

const cors = corsMiddleware({
  origins: ['http://msi', 'http://msi:8888'],
  allowHeaders: ['API-Token'],
  exposeHeaders: ['API-Token-Expiry']
})
 
server.pre(cors.preflight)
server.use(cors.actual)

module.exports = {
  server: server
}