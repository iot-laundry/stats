const sinon = require('sinon');
const request = require('supertest');

const { server } = require('../server.js');
const { influxHelper } = require('../lib/influxHelper.js');

var stubPing = null;
var stubQuery = null;

describe('Server test', function() {
  before(function(done) {
    // Mock out the influx library
    stubPing = sinon.stub(influxHelper, "testConnect").resolves([]);
    stubQuery = sinon.stub(influxHelper, "query").resolves([]);
    done();
  })

  it('Handles GET / and responds with JSON', function(done) {
    request(server)
      .get('/')
      .expect('Content-Type', /json/)
      .expect(200, done);
  })

  it('Handles GET /stats', function(done) {
    request(server)
      .get('/stats')
      .expect('Content-Type', /json/)
      .expect(200, done);
  })

  after(function(done){
    if (stubPing != null) { stubPing.restore(); }
    if (stubQuery != null) { stubQuery.restore(); }
    server.close(done);
  })
})