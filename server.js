const { server } = require('./lib/restifyHelper.js');
const { log } = require('./lib/logHelper.js');
const { influxHelper } = require('./lib/influxHelper.js');

const { indexRoute } = require('./routes/index.js');
const { statsRoute } = require('./routes/stats.js');

initInflux();
indexRoute(server);
statsRoute(server);

function initInflux() {
  influxHelper.connect();
  influxHelper.testConnect(5000);
  log.info('Influx initialization started');
}

module.exports = {
  server: server
}


